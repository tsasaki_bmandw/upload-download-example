ExtJS ファイルアップロード/ダウンロードサンプル
================================================

実行方法
--------

### Sencha

Senchaのビルドのために、extライブラリを設定
(新規にダミーのアプリを作成してextフォルダをコピー）
```
# 適当なsecha appの作成
$ sencha -sdk {SENCHA_EXT6.2_SDK_PATH} generate app -ext -classic MyApp ./MyApp

#MyApp/extを`ExtClassicUpload/`にコピー

$ cd ExtClassicUpload/
$ sencha app watch

```

### API Server

mvnでビルド実行. http://localhost:8080/

```
$ cd server
$ mvn jetty:run
```

### リバースプロキシ

[Caddy](https://caddyserver.com/)を使用。port 9090で起動。 
/apiのリクエストはAPIサーバーにプロキシする。

Caddyは開発に使えるシンプルなWebサーバ。単一の実行ファイルで簡単なリバースプロキシの機能があるので開発に便利。
カレントディレクトリにあるCaddyfileで設定。caddyコマンドで起動。


```
ブラウザ ==>  caddy:9090  /api ==> jetty:8080 (APIサーバー)
                          /    ==> ExtClassicUplod/内のファイル(静的HTML,JSファイル)
```

動作確認
--------

- 起動
    - sencha app watch
    - mvn jetty:run
    - caddy

- ブラウザでlocalhost:9090にアクセスして動作確認
- Senchaのファイル修正
    - sencha app watchがファイル変更検出でビルド実行
    - 静的ファイルの変更なのでサーバの再起動不要。ブラウザのリロードのみ
- Java側のAPIプログラム修正
    - jetty(tomcat)の再起動(ホットリロードで動かしていれば自動リロード）
    - sencha, caddyの再起動は不要
    - ブラウザのリロードも不要(ログインセッションなどがなければ)


ファイルの構成
--------------

NetBeannsのプロジェクトとしてserver作成。mavenプロジェクトなのでmvnコマンドでビルド可。

クライアントサイドはSencha Architectのプロジェクトを `ExtClassicUpload` に作成。

※gitリポジトリ登録時に extフォルダは除外しているので別途用意すること。


サーバーサイドの実装
-----------------

maven webプロジェクトで作成。Servlet API 3.0のアップロード・ダウンロードの仕様で作成。

Sencha ExtJSの独自の仕様として、アップロード後のレスポンスはContent-Type: text/plainでjsonを返し、success: trueを含める必要がある。

<http://docs.sencha.com/extjs/6.2.0/classic/Ext.form.Basic.html#method-hasUpload>

ダウンロードはSencha特に関係なし。application/octet-streamでダウンロードデータをレスポンスする。


クライアントサイド(ExtJS)の実装
------------------------------

### アップロード

- form panel内に、File Fieldを配置 name属性がmultipartのnameになる。
- アップロードしたいタイミングで、submit
- successのコールバックでサーバー側のレスポンスを受け取れる。


### ダウンロード

Senchaとしては特別なことはしておらず、ダウンロードしたいURLをtarget=_blankで開くだけ。

ボタンにhref属性をつける。このときのURLはダウンロードしたいファイルに合わせて生成されることが多いため、
サンプルでは、URLをViewModelで組み立てて、ボタンのhref属性にbindしている。





