package bmandw.uploadsample;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ダウンロード用サーブレットサンプル(Servlet API 3.0)
 * @author tsasaki
 */
@WebServlet(name = "DownloadServlet", urlPatterns = {"/download"})
public class DownloadServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("application/octet-stream");    //ダウンロード用ヘッダ
        
        // ダウンロード時のファイル名の付けるヘッダはブラウザごとの対応でハマりがち。
        response.addHeader("Content-Disposition", "attachment; filename=\"download.txt\"");
        
        String fileid = request.getParameter("id");
        
        //指定されたIDから保存されたファイルをダウンロード。
        //ダウンロードデータは、ファイルからでなくても、何らかの方法でデータを作って返してもよい。
        
        //パスのチェック要注意（サーバー上の任意のファイルをダウンロードされないように）
        try(FileInputStream fis = new FileInputStream("/tmp/" + fileid);  
            OutputStream os = response.getOutputStream())
        {   
            //ファイルから読んでOutputStreamに出力→ダウンロードへのレスポンス
            byte[] buff = new byte[1024];
            int len;
            while((len = fis.read(buff)) > 0){
                os.write(buff, 0, len);
            }
            
            
        }
    }

}
