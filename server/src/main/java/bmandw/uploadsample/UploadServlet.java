
package bmandw.uploadsample;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 * アップロードサーブレットのサンプル Servlet API 3.0
 * @author tsasaki
 */
@WebServlet(name = "UploadServlet", urlPatterns = {"/upload"})
@MultipartConfig
public class UploadServlet extends HttpServlet {

    /**
     * ファイルのポストアップロード
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        Part part = request.getPart("file1");   //フィールドのname属性
        for (String n: part.getHeaderNames()){
            System.out.printf("%s: %s\n", n, part.getHeader(n));
        }
        
        long id = System.currentTimeMillis();   //適切なID発行（推測されるIDに注意）

        /*
        マルチパートファイルデータを読み込んでサーバ上のファイルに保存する例
        ファイルデータを読んでどうするかは、任意の実装
        ・ファイルを読んでDBに保存
        ・ファイルをストレージに保存
        ・ファイルを読んでなにか処理をする
        今回はストレージ上の/tmpに保存している例だが、
        サーバに配置する場合は任意のパスに配置されないように注意が必要。
        */
        try (InputStream is = part.getInputStream();
            FileOutputStream fw = new FileOutputStream("/tmp/" + id))
        {
            byte[] buff = new byte[1024];
            int len;
            while((len = is.read(buff)) > 0){
                fw.write(buff, 0, len);
            }
        }

        //Senchaのアップロードの仕様でjsonレスポンスをtext/plainで返す必要がある
        response.setContentType("text/plain");
        
        try (PrintWriter out = response.getWriter()){
            //jsonレスポンス (successは必須）
            //文字列の組み立てで実装しているが本来はもっとちゃんとJSONデータを生成すること。
            out.printf("{\"receive\": %d, \"success\": true, \"fileid\": \"%d\"}", part.getSize(), id);
        }
    }


}
